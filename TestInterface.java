public class TestInterface {
    public static void main(String[] args) {

        Detailable[] myDetailables = new Detailable[3];

        myDetailables[0] = new SavingsAccount("Savings", 100.50);
        myDetailables[1] = new CurrentAccount("Current", 400.75);
        myDetailables[2] = new HomeInsurance(1000, 200, 800);

        for(int i = 0; i < myDetailables.length; i++){

            System.out.println(myDetailables[i].getDetails());
        }
        
    }
}