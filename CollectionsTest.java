import java.util.*;

public class CollectionsTest {
    
    public static void main(String[] args) {
        
        ArrayList<Account> accountList = new ArrayList<Account>();
        //HashSet<Account> accountList = new HashSet<Account>();
        accountList.add(new SavingsAccount("Salma", 100));
        accountList.add(new CurrentAccount("Adam", 90));
        accountList.add(new SavingsAccount("Ray", 60));

        for(Account current: accountList
    ){
            System.out.println("Old: " + current.getName() +" " + current.getBalance());
            current.addInterest();
            System.out.println("New: " + current.getName() +" " + current.getBalance());
            
        }

        //For each would use lambda

        
    }

}