public class TestInheritance {
    public static void main(String[] args) {
        
        Account[] accounts;
        accounts = new Account[2];

        accounts[0] = new SavingsAccount("Second", 4);
        accounts[1] = new CurrentAccount("Third", 6);

        for(int i = 0; i < accounts.length; i++){
            System.out.println("Old: " + accounts[i].getBalance());
            accounts[i].addInterest();
            System.out.println("New: " + accounts[i].getBalance());
        }


    }
}