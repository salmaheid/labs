public abstract class Account implements Detailable{
    
    private double balance;
    private String name;
    private static double interestRate;

    static {
        interestRate = 1.2;
    }

    public Account() {
        this("Salma", 50);
    }

    public Account (String name, double balance) {
        this.balance = balance;
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void addInterest();
        //this.balance = this.balance + (this.balance*interestRate);


    

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public boolean withdraw(double amount){

        double oldBalance;
        if (this.balance < amount){
            System.out.println("Not enough balance");
            return false;
        }
        else{
            oldBalance  = this.balance;
            this.balance = this.balance - amount;
            System.out.println("Enough balance");
            System.out.println("Old balance: " + oldBalance + ", New balance: " + this.balance);
            return true;
        }

    }
    public boolean withdraw(){
        return(this.withdraw(20));
    }

    @Override
    public String getDetails() {
        // TODO Auto-generated method stub
        return ""+ balance + " " + name;
    }
 
}