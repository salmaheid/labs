public class Car{

    //properties
    //private: access within the class
    private String make;
    private String model;
    private static double interestRate = 0.2;

    //Default constructor
    public Car (){
        
    }
    //constructor
    public Car (String make, String model){
        this.make = make;
        this.model = model;
    }
    
    
    //Getters and Setters
    //right click source action

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    
    
    
}